class Comment < ApplicationRecord
  belongs_to :post
  has_rich_text :content
  has_many_attached :images
  validates :content, :presence => true
end
